import { ServiceProvider, ServiceProviderManager } from "../src/services";

class MockedProvider extends ServiceProvider { }
class Mocked_A_SubclassProvider extends MockedProvider { }
class Mocked_B_SubclassProvider extends MockedProvider { }
class Mocked_C_SubclassProvider extends MockedProvider { }
class Mocked_D_SubclassProvider extends MockedProvider { }
class Mocked_NotYetRegistered_SubclassProvider extends MockedProvider { }

abstract class AbstractMockedProvider extends ServiceProvider {
    abstract run();
}
class MockedAbstract_E_SubclassProvider extends AbstractMockedProvider {
    run() { }
}
class MockedAbstract_F_SubclassProvider extends AbstractMockedProvider {
    run() { }
}

describe('service-provider', () => {
    let manager: ServiceProviderManager;

    beforeEach(() => {
        manager = new ServiceProviderManager();
        manager.register(new MockedProvider());
        manager.register(new Mocked_A_SubclassProvider());
        manager.register(new Mocked_B_SubclassProvider());
        manager.register(new Mocked_C_SubclassProvider());
        manager.register(new Mocked_D_SubclassProvider());
        manager.register(new MockedAbstract_E_SubclassProvider());
        manager.register(new MockedAbstract_F_SubclassProvider());
    });

    it('should return correct provider name (same as class name)', () => {
        const service = manager.resolve(MockedAbstract_E_SubclassProvider);
        expect(service?.name()).toBe('MockedAbstract_E_SubclassProvider');
    });

    it('should return correct injected service names', () => {
        expect(manager.serviceNames()).toEqual([
            'MockedProvider',
            'Mocked_A_SubclassProvider',
            'Mocked_B_SubclassProvider',
            'Mocked_C_SubclassProvider',
            'Mocked_D_SubclassProvider',
            'MockedAbstract_E_SubclassProvider',
            'AbstractMockedProvider',
            'MockedAbstract_F_SubclassProvider',
        ]);
    });

    it('should return correct providers for nested abstract classes', () => {
        const providers = manager.providers<AbstractMockedProvider>(AbstractMockedProvider);
        expect(providers.length).toBe(2);
        expect(providers.map(x => x.name())).toEqual([
            'MockedAbstract_E_SubclassProvider',
            'MockedAbstract_F_SubclassProvider'
        ]);
    });

    it('should return correct providers for not abstract classes', () => {
        const providers = manager.providers<MockedProvider>(MockedProvider);
        console.log()
        expect(providers.length).toBe(5);
        expect(providers.map(x => x.name())).toEqual([
            'MockedProvider',
            'Mocked_A_SubclassProvider',
            'Mocked_B_SubclassProvider',
            'Mocked_C_SubclassProvider',
            'Mocked_D_SubclassProvider'
        ]);
    });

    it('should register services correctly', () => {
        const addedService = new Mocked_NotYetRegistered_SubclassProvider();
        expect(manager.resolve(Mocked_NotYetRegistered_SubclassProvider)).toBe(null);
        manager.register(addedService);
        expect(manager.resolve(Mocked_NotYetRegistered_SubclassProvider)).toBe(addedService);
    });

    it('should resolve services correctly', () => {
        expect(manager.resolve(Mocked_A_SubclassProvider)).not.toBe(null);
        expect(manager.resolve(Mocked_B_SubclassProvider)).not.toBe(null);
        expect(manager.resolve(Mocked_NotYetRegistered_SubclassProvider)).toBe(null);
    });

    it('should resolve with exact service asked for', () => {
        const providerFound = manager.resolve(Mocked_A_SubclassProvider);
        expect(providerFound).not.toBe(null);
        expect(providerFound?.name()).toBe('Mocked_A_SubclassProvider');
    });

    it('should resolve with first listed service if multiple providers available', () => {
        const providerFound = manager.resolve(AbstractMockedProvider);
        expect(providerFound?.name()).toBe('MockedAbstract_E_SubclassProvider');
    });

    it('should unregister services correctly', () => {
        const addedService = new Mocked_NotYetRegistered_SubclassProvider();

        // Register service, and check if it is available
        manager.register(addedService);
        const providerFound_1 = manager.resolve(Mocked_NotYetRegistered_SubclassProvider);
        expect(providerFound_1).toBe(addedService);

        // Unregister service, and check if it is no longer available
        manager.unregister(addedService);
        const providerFound_2 = manager.resolve(Mocked_NotYetRegistered_SubclassProvider);
        expect(providerFound_2).toBe(null);
    });
});